#!/usr/bin/env bash

#APPS+=("chromium")

chromium::update_repository_cache() {
  fetchGithub https://api.github.com/repos/clickot/ungoogled-chromium-binaries/releases/latest | toManifest '.tag_name' '^.+\\.AppImage$'
}

chromium::install() {
  install_appimage chromium
}