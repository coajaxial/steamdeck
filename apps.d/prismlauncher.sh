#!/usr/bin/env bash

APPS+=("prismlauncher")

prismlauncher::update_repository_cache() {
  fetchGithub https://api.github.com/repos/PrismLauncher/PrismLauncher/releases | latestRelease | toManifest '.tag_name' '\\.AppImage$'
}

prismlauncher::install() {
  install_appimage prismlauncher
}