#!/usr/bin/env bash

APPS+=("protonupqt")

protonupqt::update_repository_cache() {
  fetchGithub https://api.github.com/repos/DavidoTek/ProtonUp-Qt/releases | latestRelease | toManifest '.tag_name | sub("^v";"")' '\\.AppImage$'
}

protonupqt::install() {
  mkdir -p /home/deck/.steam/root/compatibilitytools.d
  install_appimage protonupqt
}
