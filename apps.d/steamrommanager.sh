#!/usr/bin/env bash

APPS+=("steamrommanager")

steamrommanager::update_repository_cache() {
  fetchGithub https://api.github.com/repos/SteamGridDB/steam-rom-manager/releases | latestRelease | toManifest '.tag_name | sub("^v";"")' '(?<!i386)\\.AppImage$'
}

steamrommanager::install() {
  install_appimage steamrommanager
}