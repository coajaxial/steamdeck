#!/usr/bin/env bash

APPS+=("yuzu")

yuzu::update_repository_cache() {
  fetchGithub https://api.github.com/repos/yuzu-emu/yuzu-mainline/releases | latestRelease | toManifest '.tag_name | sub("^mainline-0-";"")' '\\.AppImage$'
}
yuzu::install() {
  install_appimage yuzu
}