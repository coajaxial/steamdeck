#!/usr/bin/env bash

APPS+=("jdk17")

jdk17::update_repository_cache() {
  date=$(date -d "$(curl -s --head https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.tar.gz.sha256 | grep -oP '(?<=Last-Modified: )[^\r+]+')" "+%s")
  jq -n --arg date "$date" --arg version 'latest' --arg url "https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.tar.gz" '{version: $version, date: $date | tonumber, url: $url}'
}
jdk17::install() {
  tar --strip-components=1 -zxvf "jdk17/download.tmp" -C "jdk17" | zenity --title="Extracting" --pulsate --progress --auto-close --auto-kill
}