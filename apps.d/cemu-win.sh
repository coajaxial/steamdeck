#!/usr/bin/env bash

APPS+=("cemu-win")

cemu-win::update_repository_cache() {
  fetchGithub https://api.github.com/repos/cemu-project/Cemu/releases | latestRelease | toManifest '.tag_name | sub("^v";"")' '-windows-x64\\.zip$'
}

cemu-win::install() {
  TMP_FILES+=("cemu-win/temp")
  mkdir cemu-win/temp
  unzip cemu-win/download.tmp -d cemu-win/temp
  rsync -ac cemu-win/temp/*/* cemu-win/
}
