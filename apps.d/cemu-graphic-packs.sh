#!/usr/bin/env bash

APPS+=("cemu-graphic-packs")

cemu-graphic-packs::update_repository_cache() {
  fetchGithub https://api.github.com/repos/cemu-project/cemu_graphic_packs/releases/latest | toManifest '.tag_name | sub("^Github";"")' 'graphicPacks\\d+\\.zip$'
}

cemu-graphic-packs::install() {
  mkdir -p cemu-win/graphicPacks/downloadedGraphicPacks
  rm -rf cemu-win/graphicPacks/downloadedGraphicPacks/*
  unzip cemu-graphic-packs/download.tmp -d cemu-win/graphicPacks/downloadedGraphicPacks/
  version=$(jq -r '.version' < cemu-graphic-packs/.latest.json)
  echo "Cemu Graphic Packs: v$version" > cemu-win/graphicPacks/downloadedGraphicPacks/version.txt
}
