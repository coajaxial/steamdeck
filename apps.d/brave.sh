#!/usr/bin/env bash

#APPS+=("brave")

brave::update_repository_cache() {
  fetchGithub https://api.github.com/repos/srevinsaju/Brave-Appimage/releases/latest | toManifest '.tag_name | sub("^v";"")' '^.+\\.AppImage$'
}

brave::install() {
  install_appimage brave
}