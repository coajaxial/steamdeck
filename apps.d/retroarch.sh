#!/usr/bin/env bash

APPS+=("retroarch")

retroarch::update_repository_cache() {
  json=$(curl -s 'https://buildbot.libretro.com/stable/?' -X POST -H 'Content-Type: application/json;charset=utf-8' --data-raw '{"action":"get","items":{"href":"/stable/","what":1}}' | jq '[.items[] | select(.href|test("^/stable/.+$"))] | sort_by(.time) | last')
  date=$(echo "$json" | jq -r '.time')
  date=$((date / 1000))
  path=$(echo "$json" | jq -r '.href')
  version=$(echo "$path" | sed -r 's/^\/stable\/([^\/]+)\/$/\1/')
  jq -n --arg date "$date" --arg version "$version" --arg url "https://buildbot.libretro.com""$path""linux/x86_64/RetroArch.7z" '{version: $version, date: $date | tonumber, url: $url}'
}

retroarch::install() {
  TMP_FILES+=("retroarch/RetroArch-Linux-x86_64")
  stdbuf -o0 7z x "retroarch/download.tmp" -o"retroarch" -bsp1 -bso0 -bse0 \
    | stdbuf -o0 tr '\b' '\n' \
    | sed -u 's/.* \{1,2\}\([0-9]\{1,3\}\)%.*/\1\n#Extracting... \1%/; s/^20[0-9][0-9].*/#Done./' \
    | zenity --title="Extracting" --progress --auto-close --auto-kill
  mv "retroarch/RetroArch-Linux-x86_64/RetroArch-Linux-x86_64.AppImage" "retroarch/retroarch"
  if [ ! -d "retroarch/retroarch.home" ]; then
    mv "retroarch/RetroArch-Linux-x86_64/RetroArch-Linux-x86_64.AppImage.home" "retroarch/retroarch.home"
  fi
}