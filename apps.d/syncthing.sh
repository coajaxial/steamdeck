#!/usr/bin/env bash

APPS+=("syncthing")

syncthing::update_repository_cache() {
  fetchGithub https://api.github.com/repos/syncthing/syncthing/releases/latest | toManifest '.tag_name | sub("^v";"")' '^syncthing-linux-amd64-.+\\.tar\\.gz$'
}

syncthing::install() {
  systemctl --user stop syncthing || true
  TMP_FILES+=("syncthing/temp")
  mkdir -p syncthing/temp
  tar --strip-components=1 -zxvf "syncthing/download.tmp" -C "syncthing/temp"
  mv "syncthing/temp/syncthing" "syncthing/syncthing"
  mkdir -p "$HOME/.config/systemd/user"
  cat >"$HOME/.config/systemd/user/syncthing.service" <<-EOM
[Unit]
Description=Syncthing - Open Source Continuous File Synchronization
Documentation=man:syncthing(1)
StartLimitIntervalSec=60
StartLimitBurst=4

[Service]
ExecStart=$SCRIPT_DIR/syncthing/syncthing serve --no-browser --no-restart --logflags=0 --no-default-folder --config=$SCRIPT_DIR/syncthing/config --data=$SCRIPT_DIR/syncthing/data
Restart=on-failure
RestartSec=1
SuccessExitStatus=3 4
RestartForceExitStatus=3 4

# Hardening
SystemCallArchitectures=native
MemoryDenyWriteExecute=true
NoNewPrivileges=true

# Elevated permissions to sync ownership (disabled by default),
# see https://docs.syncthing.net/advanced/folder-sync-ownership
#AmbientCapabilities=CAP_CHOWN CAP_FOWNER

[Install]
WantedBy=default.target
EOM
  systemctl --user enable syncthing
  systemctl --user start syncthing
}