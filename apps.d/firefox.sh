#!/usr/bin/env bash

APPS+=("firefox")

firefox::update_repository_cache() {
  fetchGithub https://api.github.com/repos/srevinsaju/Firefox-AppImage/releases/tags/firefox | toManifest '.name | sub("^Latest Continous build for firefox v";"") | sub("\\.r\\d{14}";"")' '\\.AppImage$' '.name | sub("^Latest Continous build for firefox v.*r";"") | strptime("%Y%m%d%H%M%S") | todateiso8601 | fromdateiso8601'
}

firefox::install() {
  install_appimage firefox
}