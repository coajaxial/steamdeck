#!/usr/bin/env bash

APPS+=("cryoutilities")

cryoutilities::update_repository_cache() {
  fetchGithub https://api.github.com/repos/CryoByte33/steam-deck-utilities/releases | jq '[.[] | select(.name != "latest")] | first' | toManifest '.name' '^cryo_utilities$'
}

cryoutilities::install() {
  mv "cryoutilities/download.tmp" "cryoutilities/cryoutilities"
  chmod +x "cryoutilities/cryoutilities"
  mkdir -p "$HOME/.cryo_utilities"
}