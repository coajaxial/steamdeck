#!/usr/bin/env bash

#APPS+=("cemu")

cemu::update_repository_cache() {
  fetchGithub https://api.github.com/repos/cemu-project/Cemu/releases | latestRelease | toManifest '.tag_name | sub("^v";"")' '\\.AppImage$'
}

cemu::install() {
  install_appimage cemu
}
