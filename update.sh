#!/usr/bin/env bash

set -e

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

TMP_FILES=()
cleanup() {
  rm -rf "${TMP_FILES[@]}"
}
trap cleanup 0

setup() {
  if [ ! -f .github_token ]; then
    export GITHUB_ACCESS_TOKEN=$(zenity --entry --title="GitHub access token" --text="Enter GitHub access token:")
    echo "$GITHUB_ACCESS_TOKEN" >.github_token
  else
    export GITHUB_ACCESS_TOKEN=$(cat .github_token)
  fi
}

tmp_file() {
  local tmp
  tmp=$(mktemp)
  TMP_FILES+=("$tmp")
  echo "$tmp"
}

loadApps() {
  for f in apps.d/[!_]*.sh; do
    # shellcheck disable=SC1090
    . "$f"
  done
}

fetchGithub() {
  wget -q --header="Authorization: token $GITHUB_ACCESS_TOKEN" -O- "$1"
}

latestRelease() {
  jq '.[0]'
}

toManifest() {
  local date
  date='.published_at | fromdateiso8601'
  if [ -n "$3" ]; then
    date="$3"
  fi
  jq ". | {version: $1, date: $date, url: [.assets[] | select(.name|test(\"$2\"))] | first | .browser_download_url}"
}

install_appimage() {
  mv "$1/download.tmp" "$1/$1"
  chmod +x "$1/$1"
  mkdir -p "$1/$1.home"
}

create_folders() {
  for app in "${APPS[@]}"; do
    mkdir -p "$app"
  done
}

update_repository_cache() {
  for app in "${APPS[@]}"; do
    repositoryCacheFile="$app"/.latest.json
    TMP_FILES+=("$repositoryCacheFile")
    "$app"::update_repository_cache >"$repositoryCacheFile" &
  done
  wait
}

check_for_updates() {
  options=()
  for app in "${APPS[@]}"; do
    latestFile="$app"/.latest.json
    if [ -f "$latestFile" ] && [ -s "$latestFile" ]; then
      currentFile="$app"/.current.json
      if [ ! -f "$currentFile" ]; then
        options+=("FALSE" "$app" "-" "$(jq -r '.version' <"$latestFile")" "$(date -d @"$(jq -r '.date' <"$latestFile")")")
      elif (($(jq -r '.date' <"$currentFile") < $(jq -r '.date' <"$latestFile"))); then
        options+=("TRUE" "$app" "$(jq -r '.version' <"$currentFile")" "$(jq -r '.version' <"$latestFile")" "$(date -d @"$(jq -r '.date' <"$latestFile")")")
      fi
    fi
  done
  zenity --width=1280 --height=800 --list --checklist --title="Options" \
    --text="Select your features" \
    --column="" \
    --column="App" \
    --column="Old version" \
    --column="New version" \
    --column="Date" \
    "${options[@]}"
}

download() {
  app="$1"
  repositoryCacheFile="$app"/.latest.json
  url=$(jq -r '.url' <"$repositoryCacheFile")
  version=$(jq -r '.version' <"$repositoryCacheFile")
  TMP_FILES+=("$app/download.tmp")
  wget --header="Authorization: token $GITHUB_ACCESS_TOKEN" -q --show-progress --progress=bar:noscroll -O "$app/download.tmp" "$url" 2>&1 |
    sed -u 's/^[a-zA-Z\-].*//; s/.* \{1,2\}\([0-9]\{1,3\}\)%.*/\1\n#Downloading... \1%/; s/^20[0-9][0-9].*/#Done./' |
    zenity --title="$app $version" --progress --auto-close --auto-kill
}

cd "$(dirname "${BASH_SOURCE[0]}")"

APPS=()
setup
loadApps
create_folders
update_repository_cache
appsToUpdate=$(check_for_updates)
IFS='|' read -r -a appsToUpdate <<<"$appsToUpdate"
for app in "${appsToUpdate[@]}"; do
  download "$app"
  "$app"::install
  latestFile="$app"/.latest.json
  currentFile="$app"/.current.json
  mv "$latestFile" "$currentFile"
done
zenity --info --text "Update/install complete."

exit 0
